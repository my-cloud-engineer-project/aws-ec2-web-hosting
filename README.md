# aws-ec2-web-hosting

## A repeat of the static web hosting project, but with EC2.

- Deploy a EC2 VM and host a simple static "Fortune-of-the-Day Coming Soon" web page.  
   ** You deploy a t2.micro with defaults.  
   ** Apply Security Group with http https and ssh ports open. 
   ** Run  
   ``` aws-apache-bootstrap.sh ```  
   ** navigate to provided public ip in browser
   ** enjoy static website on Apache

- Take a snapshot of your VM, delete the VM, and deploy a new one from the snapshot. Basically disk backup + disk restore.

- Checkpoint: You can view a simple HTML page served from your EC2 instance.


- Create an AMI from that VM and put it in an autoscaling group so one VM always exists.

- Put a Elastic Load Balancer infront of that VM and load balance between two Availability Zones (one EC2 in each AZ).

- Checkpoint: You can view a simple HTML page served from both of your EC2 instances. You can turn one off and your website is still accessible.